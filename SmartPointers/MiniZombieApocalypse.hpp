#pragma once

#include <ctime>
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <vector>

#include <boost/shared_ptr.hpp>

namespace MiniZombieApocalypse
{

class Virus
{
public:
  Virus() = default;
  ~Virus() = default;
};

class Human
{

class Deleter
{
public:
  void operator()(Human* p)
  {
    // std::cout << "Human " <<  p->number << " DIED" << std::endl;
  }
};

friend class Deleter;

protected:
  Human(uint32_t n): virus(nullptr), number(n){};
  virtual ~Human() {};

public:

  static boost::shared_ptr<Human> getHuman(const uint32_t number)
  {
    boost::shared_ptr<Human> p(new Human(number), Human::Deleter());
    return p;
  }

  void interract(Human& target)
  {
    if(virus.get() == nullptr)
      return;

    std::cout << "I " <<  number << " interract with " <<  target.getNumber() << std::endl;
    target.receive(virus);
  }

  void receive(boost::shared_ptr<Virus>& v)
  {
    virus = v;
  }

  void getInfected(Virus* v)
  {
    std::cout << "I " <<  number << " got infected" << std::endl;
    virus = boost::shared_ptr<Virus>(v);
  }

  bool isInfected() const
  {
    return virus.get() != nullptr;
  }

  uint32_t getNumber() const
  {
    return number;
  }

  boost::shared_ptr<Virus> virus;
  uint32_t number;
};

class Simulation
{
public:
  Simulation() = default;
  ~Simulation() = default;

  bool isApocalypseOver(const std::vector<boost::shared_ptr<Human>>& humans) const
  {
    for (auto i = humans.begin(); i != humans.end(); ++i)
    {
      if((*i)->isInfected())
      {
        return false;
      }
    }
    return true;
  }

  void run()
  {
    std::srand(std::time(nullptr));
    std::vector<boost::shared_ptr<Human>> humans;

    for (int i = 0; i < 100; ++i)
    {
      humans.push_back(Human::getHuman(i));
      // humans.push_back(std::move(Human::getHuman(i)));
    }

    for (int i = 0; i < 10; ++i)
    {
      uint32_t humanIndex = std::rand() % humans.size();
      humans[humanIndex]->getInfected(new Virus);

      std::cout << humans[humanIndex]->getNumber() << " is first infected" << std::endl;
    }

    while(true)
    {
      for (int i = 0; i < 10; ++i)
      {
        humans[std::rand() % humans.size()]->interract(*humans[std::rand() % humans.size()]);
      }

      for (auto i = humans.begin(); i != humans.end(); ++i)
      {
        if((*i)->isInfected())
        {
          std::cout << (*i)->getNumber() << " DEAD" << std::endl;
          humans.erase(i);
          break;
        }
      }

      if(isApocalypseOver(humans))
      {
        std::cout << "Apocalypse is overe" << std::endl;
        break;
      }
    }

    if(humans.size() > 0)
    {
      std::cout << "Humanity survived" << std::endl;
    }
    else
    {
      std::cout << "Humanity died" << std::endl;
    }
  }
};

}
