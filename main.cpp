#include <iostream>
#include <BoostProjectConfig.h>

#include "SmartPointers/MiniZombieApocalypse.hpp"

int main(int argc, char *argv[])
{

    // report version
    std::cout << argv[0] << " Version " << BoostProject_VERSION_MAJOR << "."
              << BoostProject_VERSION_MINOR << std::endl;
    std::cout << "Usage: " << argv[0] << " number" << std::endl;

    MiniZombieApocalypse::Simulation sim;
    sim.run();

    return 0;
}